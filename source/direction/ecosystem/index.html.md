---
layout: markdown_page
title: Product Vision - Ecosystem
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Ecosystem.

We are hiring for a [Senior Product Manager](https://about.gitlab.com/jobs/apply/senior-product-manager-ecosystem-4192679002/)
to lead GitLab's vision for our ecosystem. To explore other roles, please visit our [jobs page](https://jobs.gitlab.com) to learn and apply.

## Overview

Ecosystem's mission is to foster the development of the GitLab developer ecosystem. Our ambitious [product vision](https://about.gitlab.com/direction/product-vision/) aspires to expand the breadth of the GitLab application, but it's only achievable in partnership with others. While we'll always seek to develop native capabilities inside GitLab that extend our core value proposition, there are often significant advantages associated with integrating other technologies into the GitLab platform. Our goal is to make this process seamless from both the end-user and developer perspective, allowing GitLab users to unlock value in an augment DevOps toolchain that works for their business.

## Challenges

GitLab currently [integrates with others](https://docs.gitlab.com/ee/integration/), but we aspire to allow others to integrate deeper into GitLab and with exceptional ease. Integration partners like [Percy](https://docs.percy.io/v1/docs/gitlab) seek to build with GitLab, but struggle against requirements like [needing a bot user](https://docs.percy.io/v1/docs/gitlab#section-create-a-gitlab-bot-user) and a [limited ability to scope](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token) token access.

Thinking more broadly than specific limitations, the Ecosystem group will need to consider the following:

* Which partners should we work with on developing our ecosystem? What does success look like for these partnerships, and how can we learn along the way?
* How do we design and prioritize our integration APIs? Which areas of the GitLab product do we prioritize, and for which partners?
* How do we measure success for our partners, users, and GitLab? How do we align incentives between all three, including our community contributors?
* How will we make GitLab integration easy for developers? How do we evangelize our ecosystem?
* Can we consider a strategy that serves both our base of self-managed instances as well as GitLab.com?

## Ecosystem Categories

- Integration SDK: A category focused on improving the experience and documentation regarding the developer integration experience
- Marketplace: An essential framework for improving the experience for GitLab users to utilize 3rd party technology
- Native Integrations: A category focused on GitLab maintained integrations

## Influences

We're inspired by other companies with rich, developer-friendly ecosystems like [Shopify](https://help.shopify.com/en/api/getting-started), [GitHub](https://github.com/marketplace), and their [APIs](https://developer.github.com/v3/checks/).
